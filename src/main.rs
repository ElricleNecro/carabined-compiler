use std::env;
use std::fs::File;
use std::io::Read;

fn read_file(fname: &str) -> Vec<String> {
    let mut f = File::open(fname).expect("File not found.");
    let mut content = String::new();
    f.read_to_string(&mut content).expect("There was a problem while reading the file.");

    content
        .replace("\r", "")
        .split('\n')
        .map(|x| x.to_string())
        .filter(|x| ! x.is_empty())
        .collect()
}

pub fn main() {
    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);

    let src = read_file(&args[1]);
    println!("{:?}", src);
}
